import java.util.Scanner;

public class FastFood
{
	
	public static void main(String args[])
	{
		//Declare Variables
		int numberOfHamburgers;
		int numberOfCheeseburgers;
		int numberOfSodas;
		int numberOfFries;
		String userName;
		double total;
		
		//gets input from reader
		Scanner readInputObj = new Scanner(System.in);
		
		System.out.println("How many hamburgers? ");
		numberOfHamburgers=readInputObj.nextInt();
		
		System.out.println("How many Cheeseburgers? ");
		numberOfCheeseburgers=readInputObj.nextInt();
		
		System.out.println("How many sodas? ");		
		numberOfSodas=readInputObj.nextInt();
		
		System.out.println("How many fries? ");
		numberOfFries=readInputObj.nextInt();
		
		System.out.print("Enter name: ");
		userName=readInputObj.next();
		
		//Calaculations
		total = numberOfHamburgers*1.25 + numberOfCheeseburgers*1.50 + numberOfSodas * 1.95 + numberOfFries * 0.95;
		
		//Total
		System.out.println("\nName: "+ userName.toUpperCase()+"\nTotal: $"+String.format ("%,.2f", total));
		
		
		
	}

}
