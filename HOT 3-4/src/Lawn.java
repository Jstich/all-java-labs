public class Lawn
{
	// Instance fields
	int width;
	int length;
	

	public Lawn()
	{
		this.width=0;
		this.length=0;
		
	}
	
	public Lawn(int width, int length)
	{		
		this.width = width;
		this.length = length;
	}
	
	
	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getlength()
	{
		return length;
	}

	public void setlength(int length)
	{
		this.length = length;
	}

	
	public double squareFeetOfALawn()
	{
		return (length*width);
	}
	
	
	
	
	
}

