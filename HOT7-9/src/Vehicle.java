
public class Vehicle{
    // variable
    public int speed;
    
    //Setter method for speed
    public void setSpeed(int speed){
        this.speed = speed;
    }
    //Getter method from speed
    public int getSpeed(){
        return this.speed;
    }
    
    //accelerate method
    public void accelerate(){
        this.speed+=5;
    }
}
