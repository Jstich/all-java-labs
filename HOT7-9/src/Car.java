//Car class, extending Vehicle
 public class Car extends Vehicle{
    //overriding accelerate method
    @Override
    public void accelerate(){
        super.speed+=10;
    }
}