//Truck Class, extending Vehicle
public class Truck extends Vehicle{
    //Overriding accelerate method
    @Override
    public void accelerate(){
        super.speed += 3;
    }
}
