
public class GameWithTimeLimit extends Game{
    int timeLimit;
    //Setter method for TimeLimit
    public void setTimeLimit(int timeLimit){
        this.timeLimit = timeLimit;
    }
    
    //Getter method for TimeLimit
    public int getTimeLimit(){
        return this.timeLimit;
    }
    
    //toString method
    public String toString(){
        return super.toString()+" "+this.timeLimit;
    }
    
}