
public class num3Driver{
    public static void main(String args[]){
        //Super class variable referring to subclass 
        Vehicle v = new Car();
        v.accelerate();
        v.accelerate();
        v.accelerate();
        System.out.println("Speed of car is "+v.getSpeed());
        
        // Instance for Truck
        Vehicle v2 = new Truck();
        v2.accelerate();
        v2.accelerate();
        v2.accelerate();
        System.out.println("Speed of truck is "+v2.getSpeed());
    }
}
