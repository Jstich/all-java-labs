
public class Game{
    String name;
    int maxPlayers;
    //setter for name
    public void setName(String name){
        this.name = name;
    }
    //Getter method for name
    public String getName(){
        return name;
    }
    //setter method for max players
    public void setMaxPlayers(int n){
        this.maxPlayers = n;
    }
    //Getter method for max players
    public int getMaxPlayers(){
        return maxPlayers;
    }
    //toString method
    public String toString(){
        return name+" "+maxPlayers;
    }
	
}
