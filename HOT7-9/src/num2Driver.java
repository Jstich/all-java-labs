
public class num2Driver {
    public static void main(String args[]){
        Game g1 = new Game();
        g1.setName("Hockey"); //get name of game 1
        g1.setMaxPlayers(20); //get max players
       
        System.out.println("Game 1 :   "+g1 + " players on the ice at once");
        
        GameWithTimeLimit g2 = new GameWithTimeLimit();
        g2.setName("Football"); //get name of game two
        g2.setMaxPlayers(10);//gets max players
        g2.setTimeLimit(60);//gets max time
        System.out.println("Game 2 : "+g2 + "  ");
    }
}