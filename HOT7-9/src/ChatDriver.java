import java.util.Scanner;



public class ChatDriver {
    public static void main(String args[]) {
        //Array for area codes
        int areaCode[] = {262, 414, 609, 715, 815, 920};
        //Array for cost
        double cost[] = {0.07, 0.10, 0.05, 0.16, 0.24, 0.14};
        //Scanner for reading the input
        @SuppressWarnings("resource")
		Scanner obj = new Scanner(System.in);
        //ask for the area code
        System.out.println("Enter an area code");
        //read the area code
        int code = obj.nextInt();
        //ask for  the user for time
        System.out.println("Enter the time for the call"); 
        //Read the time
        int time = obj.nextInt();
        //Iterate through all the area codes
        for (int i = 0; i < areaCode.length; i++) {
            //If area code is equal to entered code, calculate the total Cost
            if (areaCode[i] == code) {
                System.out.println("Total cost of the call is " + (time * cost[i])); //prints total cost and does the math
                return;
            }
        }
    }
}


