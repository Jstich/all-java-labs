import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;

public class Sales {
    public static void main(String[] args) throws IOException {
        //get user input
        Scanner sc = new Scanner(System.in);
        //write to a file
        PrintWriter pw = new PrintWriter("WeeklySales.txt");
        
        final int SALESPEOPLE = 5;
	    int sum = 0;
        
        for (int i=0; i<SALESPEOPLE; i++) {
            System.out.print("Enter sales for salesperson " + (i+1) + ": ");
            int sale = sc.nextInt();
            
            while (sale < 0) {
                System.out.print("Invalid input. Sale must be greater than 0. Re-enter: ");
                sale = sc.nextInt();
            }
            
            //write this sale to file
            pw.println(sale);

            sum = sum + sale;
        }
        
        //now write sum to file
        pw.println(sum);
        pw.close();
        sc.close();
    }   
    
}
