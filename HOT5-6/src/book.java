class book {
String BookName;
String PublishDate;
String Author;

public book(String n, String date, String auth){
    this.BookName = n;
    this.PublishDate = date;
    this.Author = auth;
    }
    
    public static void main(String args[]){
        book b = new book("Harry Potter", "10 Jan, 2005", "J K Rowling" );
        library l = new library("ABC", b);
        System.out.println(l.toString());
    }
}

class library {
    String LibName;
    book one; // this library class has A relation with Book class (aggregation)
    
    public library(String n, book a) {
        this.LibName = n;
        this.one = a;
    }
    
public String toString(){//overriding the toString() method  
  return "Name= "+LibName+ "Book details= "+ this.one.BookName +" " + this.one.PublishDate + " " + this.one.Author;  
 } 
}